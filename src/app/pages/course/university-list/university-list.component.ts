import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-university-list',
  templateUrl: './university-list.component.html',
  styleUrls: ['./university-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UniversityListComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
