import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { SeoService } from '../../services/seo/seo.service';
import { SEOInfo } from '../../services/seo/seo.interface';
import {
  ActivatedRoute
} from '@angular/router';

@Component({
  selector: 'app-university',
  templateUrl: './university.component.html',
  styleUrls: ['./university.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class UniversityComponent implements OnInit {
  ies_slug: String = '';
  constructor(
    private route: ActivatedRoute,
    private seoService: SeoService
  ) { }

  ngOnInit() {
    const routeParams = this.route.params['_value'];
    this.ies_slug = routeParams.ies;
    let seoData: SEOInfo = {
      title: {
        fragment: "Faculdade Única - Cursos"
      },
      metabase: {
        description: "Página da faculdade"
      }
    };

    this.seoService.setSeoData(seoData);

  }

}
