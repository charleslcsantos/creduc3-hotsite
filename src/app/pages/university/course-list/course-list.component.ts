import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: '[app-course-list]',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CourseListComponent implements OnInit {
  public showModalFilter:boolean = false;
  constructor() { }

  ngOnInit() {
  }

  teste(e) {
    console.log(e);
  }

}
