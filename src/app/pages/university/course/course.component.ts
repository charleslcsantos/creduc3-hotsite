import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UniversityCourseComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
