import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgModel } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { SeoService } from "../../services/seo/seo.service";
import { SEOInfo } from '../../services/seo/seo.interface';

@Component({
  selector: '[home]',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class HomeComponent implements OnInit {
  modalidade: String = "";
  location = {};

  constructor(
    private seoService: SeoService
  ) {}

  ngOnInit() {
    let seoData:SEOInfo = {
      title: {
        fragment: "Crédito Educativo"
      },
      metabase: {
        description: "Home Page"
      }
    };

    this.modalidade = "GRADUACAO";
    this.seoService.setSeoData(seoData);
    
  }
}