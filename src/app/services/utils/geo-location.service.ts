import { Injectable } from "@angular/core";
import { Subject } from "rxjs/Subject";
import { Observable } from "rxjs/Observable";

@Injectable()
export class GeoLocationService {
  private _location: Position;

  get location() {
    return this._location;
  }

  set location(value: any) {
    this._location = value;
  }

  constructor() {
    this
      .getLocation()
      .subscribe(
        data => {
          this.location = data;
        }, 
        error => {
          console.error('Location attr error');
        }
      )
  }

	getLocation(): Observable<Position> {
    return Observable.create((observer) => {
      if(navigator && 'geolocation' in navigator && navigator.geolocation){
        navigator.geolocation.getCurrentPosition((position) => {
          observer.next(position);
          observer.complete();
        });
      };
    })
  }
}