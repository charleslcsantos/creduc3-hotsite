import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: '[app-parcel-item]',
  templateUrl: './parcel-item.component.html',
  styleUrls: ['./parcel-item.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ParcelItemComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
