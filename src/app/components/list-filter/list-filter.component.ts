import { Component, OnInit, ViewEncapsulation, Output, EventEmitter } from '@angular/core';

@Component({
  selector: '[app-list-filter]',
  templateUrl: './list-filter.component.html',
  styleUrls: ['./list-filter.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ListFilterComponent implements OnInit {
  @Output() show = new EventEmitter<boolean>();
  showModalFilter: false;

  constructor() { }

  ngOnInit() {
  }

  closeModal() {
    this.show.emit(false);
  }

}
