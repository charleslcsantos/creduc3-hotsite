import { Component, OnInit, Input, ViewEncapsulation} from '@angular/core';
import { ClassGetter } from '@angular/compiler/src/output/output_ast';
import { NgxCarousel } from 'ngx-carousel';


interface IFeatureItensConfig {
  title: string,
  locationLabel: string,
  searchPlaceholder: string,
}

interface FeaterImageModel {
  src: string,
  title: string
}

interface FeatureItemsModel {
  title: string,
  fee: number,
  img: FeaterImageModel
}


@Component({
  selector: '[app-feature-items]',
  templateUrl: './feature-itens.component.html',
  styleUrls: ['./feature-itens.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class FeatureItensComponent implements OnInit {
  @Input() location: boolean = false;
  @Input() search: boolean = false;
  @Input() title: string = '';
  @Input() type: string = 'ies'; // 'ies' -> universidades  | 'cursos' -> cursos
  showButton: boolean = true;

  public carousel: NgxCarousel;

  ies: IFeatureItensConfig = {
    locationLabel: 'Instituições próximas a',
    title: 'Universidades e Faculdades que oferecem parcelamento',
    searchPlaceholder: 'Busque por uma faculdade específica',
  };
  
  curso: IFeatureItensConfig = {
    locationLabel: 'Cursos',
    title: 'Confira os melhores cursos com opções de parcelamento',
    searchPlaceholder: 'Busque por um curso específico',
  }

  private configList = {
    ies: this.ies,
    curso: this.curso
  };
  
  public config: IFeatureItensConfig;
  public listItems: Array<FeatureItemsModel> = [];

  constructor() { }

  ngOnInit() {
    if (this.type == 'ies') {
      this.config = this.configList.ies;
      this.showButton = true;
      this.carousel = {
        grid: {xs: 1, sm: 3, md: 3, lg: 4, all: 0},
        slide: 4,
        speed: 400,
        interval: 4000,
        point: {
          visible: false
        },
        load: 2,
        touch: true,
        loop: false,
        custom: 'banner'
      }

      for (let i = 0; i < 16; i++) {
        this.listItems.push({
          title: 'Faculdade Estácio de Sá',
          fee: 50,
          img: {
            src: "assets/images/faculdades/logo-estacio.png",
            title: "Logo da Faculdade Estácio de Sá"
          }
        })
      }

    } else {
      this.config = this.configList.curso;
      this.showButton = false;
      this.carousel = {
        grid: {xs: 1, sm: 2, md: 3, lg: 3, all: 0},
        slide: 3,
        speed: 400,
        interval: 4000,
        point: {
          visible: false
        },
        load: 3,
        touch: true,
        loop: false,
        custom: 'banner'
      }

      for (let i = 0; i < 9; i++) {
        this.listItems.push({
          title: 'Administração',
          fee: 70,
          img: {
            src: "assets/images/faculdades/logo-estacio.png",
            title: "Logo do curso de adm"
          }
        })
      }
    }

    if (this.title == '') {
      this.title = this.config.title;
    }
  }

}
