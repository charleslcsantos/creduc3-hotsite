import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: '[app-brand-card]',
  templateUrl: './brand-card.component.html',
  styleUrls: ['./brand-card.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BrandCardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
