import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: '[app-fies-comparison]',
  templateUrl: './fies-comparison.component.html',
  styleUrls: ['./fies-comparison.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FiesComparisonComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
