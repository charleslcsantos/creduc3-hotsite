
import { Input, Component, OnInit, ViewEncapsulation, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ClassGetter } from '@angular/compiler/src/output/output_ast';

import { NgbTypeaheadConfig } from '@ng-bootstrap/ng-bootstrap';

import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';

import { ISearchService } from '../../services/search/search.interface';
import { CourseSearchService } from './../../services/search/search-course.service';
import { IesSearchService } from './../../services/search/search-ies.service';


@Component({
  selector: '[app-form-filtro-curso]',
  templateUrl: './form-filtro-curso.component.html',
  styleUrls: ['./form-filtro-curso.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [NgbTypeaheadConfig]
})
export class FormFiltroCursoComponent implements OnInit {

  @Input() modalidade: any;

  private searchService: ISearchService;

  public term: any;
  public searchType: any;
  public courseType: any;

  searching = false;
  searchFailed = false;
  hideSearchingWhenUnsubscribed = new Observable(() => () => this.searching = false);

  constructor(
    private route: Router,
    private courseSearchService: CourseSearchService,
    private iesSearchService: IesSearchService
  ) {
    this.searchService = this.courseSearchService;
  }

  ngOnInit() {
    this.searchType = 'curso';
    this.courseType = {
      presencial: {
        flag: 'P',
        checked: true
      },
      ead: {
        flag: 'E',
        checked: true
      }
    }
  }

  changeSearchType() {
    this.searchService = this.searchType === "ies" ? this.iesSearchService : this.courseSearchService;
  }
  
  typing = (text$: Observable<string>) =>
    text$
      .do(() => this.searching = true)
      .debounceTime(300)
      .distinctUntilChanged()
      // .map(term => this.searchService.search(term, this))
      .switchMap(term =>
        this.searchService.search(term, this)
      )
      .do(() => this.searching = false)

  selectItem(event: any) {
    this.search(event.item);
  } 

  search(term: any) {
    let url = this.searchService.generateUrl(term);
    this.route.navigate([url]);
  }
}
