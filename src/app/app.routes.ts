import { Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { UniversityComponent } from './pages/university/university.component';
import { CourseListComponent } from "./pages/university/course-list/course-list.component";
import { UniversityCourseComponent } from "./pages/university/course/course.component";
import { AboutComponent } from "./pages/university/about/about.component";
import { CourseComponent } from "./pages/course/course.component";
import { RegistrationComponent } from "./pages/university/registration/registration.component";
import { UniversityListComponent } from "./pages/course/university-list/university-list.component";

export const ROUTES: Routes = [
	{ path: '', component: HomeComponent, pathMatch: 'full' },
	// { path: 'unica', loadChildren: './pages/university/university.module#UniversityModule' },
	{
		path: 'curso/:curso',
		component: CourseComponent
	},
	{
		path: 'localidade',
		component: CourseComponent,
		children: [
			{ path: ':local/:curso', component: UniversityListComponent }
		]
	},
	{
		path: ':ies',
		component: UniversityComponent,
		children: [
			{ path: '', component: AboutComponent },
			{ path: 'inscricao', component: RegistrationComponent },
			{ path: 'cursos', component: CourseListComponent },
			{ path: ':curso', component: UniversityCourseComponent },
		]
	}
];