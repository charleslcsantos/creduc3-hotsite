import { Component } from '@angular/core';
import { SeoService } from "./services/seo/seo.service";
import { Router } from '@angular/router';
import { GeoLocationService } from './services/utils/geo-location.service';

@Component({
  selector: 'main',
  template: `<router-outlet></router-outlet>`,
})
export class AppComponent {
  constructor(
    private seoService: SeoService,
    private router: Router,
    private geoLocationService: GeoLocationService,
  ){
    
    this.seoService.setDefaultSeo();

    this.router.events.subscribe((event) => {
      console.log('test');
      console.log(this.geoLocationService.location);
      // .subscribe((position) => {
      //   console.log(position.coords);
      // })
    })
  }
}
