import { IdNamedModel, ImagemModel } from "./base";
import { LocalidadeModel } from "./localizacao";
import { CursoModel } from "./curso";

export interface InstituicaoModel extends IdNamedModel{

    Banners?: Array<ImagemModel>;
    Logos: Array<ImagemModel>;
    Conteudo: string;
    Email?: string;
    Telefone?: string;
    Localidade?: LocalidadeModel;
    Campi?: Array<InstituicaoModel>;
    Cursos?: Array<CursoModel>;
}