import { IdNamedModel } from "./base";


export interface EstadoModel extends IdNamedModel{
    UF: string;
}

export interface CidadeModel extends IdNamedModel{
    Estado: EstadoModel;
}

export interface LocalidadeModel{
    Latitude: Number;
    Longitude: Number;
    Endereco?: string;
    Cidade?: CidadeModel;
}