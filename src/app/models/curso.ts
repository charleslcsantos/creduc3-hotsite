import { FlagableModel, IdNamedModel } from "./base";

export interface MensalidadeModel{}

export interface TurnoModel extends FlagableModel{}

export interface GrauModel extends FlagableModel{}

export interface ModalidadeModel extends FlagableModel{}

export interface TipoCursoModel extends FlagableModel{}

export interface CursoModel extends IdNamedModel{
    
    Mensalidade: MensalidadeModel;
    Turno: TurnoModel;
    Grau: GrauModel;
    Modalidade: ModalidadeModel;
    Tipo: TipoCursoModel;

}
