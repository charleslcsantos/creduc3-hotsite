

export interface IdNamedModel {
    Id?: Number;
    Nome?: String;
}

export interface FlagableModel extends IdNamedModel {
    Flag?: string;
}

export enum Tamanho {
    sm = "sm",
    md = "md",
    lg = "lg"
}

export interface ImagemModel {

    Url: string;
    Tamanho: Tamanho;

}
