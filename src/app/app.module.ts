import { ROUTES } from './app.routes';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpModule, Http, XHRBackend, RequestOptions, ConnectionBackend} from '@angular/http';

// Vendor Components
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxCarouselModule } from 'ngx-carousel';

// Modules
import { SeoModule } from "./services/seo/seo.module";
import { SearchModule } from "./services/search/search.module";
import { AbsoluteUrlService } from "./services/utils/absoluteurl.service";

// Components
import { AppComponent } from './app.component';
import { TopbarComponent } from './components/topbar/topbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { FeatureItensComponent } from './components/feature-itens/feature-itens.component';
import { BrandCardComponent } from './components/university/brand-card/brand-card.component';
import { AddressComponent } from './components/university/address/address.component';
import { FiesComparisonComponent } from './components/university/fies-comparison/fies-comparison.component';
import { StepsComponent } from './components/university/steps/steps.component';
import { FormFiltroCursoComponent } from './components/form-filtro-curso/form-filtro-curso.component';
import { ParcelItemComponent } from './components/parcel-item/parcel-item.component';
import { ListFilterComponent } from './components/list-filter/list-filter.component';

import { UniversityComponent } from './pages/university/university.component';
import { AboutComponent } from "./pages/university/about/about.component";
import { CourseListComponent } from "./pages/university/course-list/course-list.component";
import { UniversityCourseComponent } from "./pages/university/course/course.component";

import { CourseComponent } from './pages/course/course.component';
import { RegistrationComponent } from "./pages/university/registration/registration.component";
import { UniversityListComponent } from './pages/course/university-list/university-list.component';

import { HomeComponent } from './pages/home/home.component';
import { SearchResultComponent } from './pages/search-result/search-result.component';

import { ApiHttpProvider, httpFactory } from './providers/api-http.provider';
import { StorageService } from './services/storage.service';
import { HttpClientModule } from '@angular/common/http';
import { GeoLocationService } from './services/utils/geo-location.service';

@NgModule({
  declarations: [
    AppComponent,
    TopbarComponent,
    FooterComponent,
    FeatureItensComponent,
    BrandCardComponent,
    AddressComponent,
    FiesComparisonComponent,
    StepsComponent,
    FormFiltroCursoComponent,
    HomeComponent,
    SearchResultComponent,
    UniversityComponent,
    AboutComponent,
    CourseListComponent,
    UniversityCourseComponent,
    ParcelItemComponent,
    ListFilterComponent,
    CourseComponent,
    RegistrationComponent,
    UniversityListComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'my-app'}),
    RouterModule.forRoot(ROUTES),
    HttpModule,
    HttpClientModule,
    SeoModule,
    SearchModule,
    NgbModule.forRoot(),
    NgxCarouselModule,
    FormsModule
  ],
  providers: [
    AbsoluteUrlService,
    {
      provide: Http,
      useFactory: httpFactory,
      deps: [XHRBackend, RequestOptions]
    },
    {
      provide: ConnectionBackend,
      useClass: XHRBackend
    },
    ApiHttpProvider,
    StorageService,
    GeoLocationService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
